// SPDX-FileCopyrightText: 2023 Michael Pöhn <michael@poehn.at>
// SPDX-License-Identifier: GPL-3.0-or-later

import 'package:fdroidwebdash/widgets/responsive_padding.dart';
import 'package:flutter/material.dart';
import 'package:go_router/go_router.dart';
import 'package:provider/provider.dart';

import '../api/index_v2_api.dart';
import '../providers/packages_state.dart';
import '../utils/platform_specific.dart';

class LandingScreen extends StatelessWidget {
  const LandingScreen({super.key});

  @override
  Widget build(BuildContext context) {
    var packagesState = Provider.of<PackagesState>(context);
    Repo? repo = packagesState.repos.values.first;
    return SafeArea(
      child: Container(
        color: Theme.of(context).colorScheme.background,
        child: SingleChildScrollView(
          child: ResponsivePadding(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 32),
              child: Column(
                children: [
                  const SizedBox(height: 64),
                  TitleCard(repo: repo),
                  const SizedBox(height: 32),
                  Text(
                    repo.description?.get() ?? 'no repository description',
                    textAlign: TextAlign.justify,
                  ),
                  const SizedBox(height: 32),
                  FittedBox(
                    child: FilledButton(
                      child: const Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          SizedBox(
                            width: 8,
                          ),
                          Text("apps"),
                          Icon(
                            Icons.chevron_right,
                            size: 18,
                          ),
                        ],
                      ),
                      onPressed: () {
                        GoRouter.of(context).go("/packages");
                      },
                    ),
                  ),
                  const SizedBox(height: 8),
                  const Text('Explore apps in this repository.'),
                  const SizedBox(height: 64),
                  const LinksBlock(),
                  const SizedBox(height: 64),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}

class TitleCard extends StatelessWidget {
  final Repo repo;

  const TitleCard({super.key, required this.repo});

  @override
  Widget build(BuildContext context) {
    return Column(children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: ClipRRect(
              borderRadius: BorderRadius.circular(32),
              child: Container(
                color: Theme.of(context).colorScheme.surface,
                child: Column(
                  children: [
                    const SizedBox(height: 32),
                    Image.network(
                      '${repo.address}${repo.icon?.iconUrl}',
                      height: 64 + 32,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                        top: 16,
                        bottom: 32,
                        left: 16,
                        right: 16,
                      ),
                      child: Text(
                        repo.name.get() ?? 'Unnamed Repo',
                        textScaleFactor: 2,
                        maxLines: 2,
                        textAlign: TextAlign.center,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    ]);
  }
}

class LinksBlock extends StatelessWidget {
  const LinksBlock({super.key});

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
      children: [
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const Text("already using F-Droid?"),
            const SizedBox(height: 4),
            FittedBox(
              child: TextButton(
                child: const Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Icon(Icons.install_mobile, size: 18),
                    SizedBox(width: 4),
                    Text("add repo"),
                  ],
                ),
                onPressed: () {
                  GoRouter.of(context).go("/add_repo");
                },
              ),
            ),
          ],
        ),
        Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            const Text("new to F-Droid?"),
            const SizedBox(height: 4),
            FittedBox(
              child: TextButton(
                child: const Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Icon(
                      Icons.explore_outlined,
                      size: 18,
                    ),
                    SizedBox(
                      width: 2,
                    ),
                    Text("learn more"),
                    SizedBox(
                      width: 2,
                    ),
                  ],
                ),
                onPressed: () {
                  platformSpecificUtils()
                      .openUrl(fileUrl: "https://f-droid.org");
                },
              ),
            ),
          ],
        ),
      ],
    );
  }
}
